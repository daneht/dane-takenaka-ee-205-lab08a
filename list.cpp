///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
///
/// @file list.cpp
/// @version 1.0
///
/// Implementation file for list
///
/// @author Dane Takenaka <danaeht@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   4/6/2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <list>

#include "node.hpp"
#include "list.hpp"
void DoubleLinkedList::insertionSort() {
   DoubleLinkedList sorted;
   if ( head == nullptr ) {
      std::cout << "List is empty" << std::endl;
      return;
   }
}
const bool DoubleLinkedList::isSorted() const {
   unsigned int count = 0;
   if( count <= 1 )
      return true;
   for( Node* i = head ; i -> next != nullptr; i = i -> next ) {
      if( *i > *i -> next )
         return false;
   }
   return true;
}
void DoubleLinkedList::swap( Node* node1, Node* node2 ) {
   Node* A = node1 -> next;
   Node* B = node1 -> prev;
   Node* C = node2 -> next;
   Node* D = node2 -> prev;
   if( node1 == node2 ) {
      std::cout << "Nodes are same" << std::endl;
      return;
   }
   if (( node1 -> prev == nullptr ) || ( node2 -> next == nullptr )) {
      node2 -> prev = nullptr;        
      node2 -> next = node1 -> next;   
      A -> prev = node2;              
      head = node2;                   
      node1 -> next = nullptr;        
      node1 -> prev = D;              
      D -> next = node1;              
      tail = node1;                    
      return;
   }
   if (( node1 -> next == nullptr ) || ( node2 -> prev == nullptr )) {
      node1 -> prev = nullptr;        
      node1 -> next = node2 -> next;   
      C -> prev = node1;              
      head = node1;                   
      node2 -> next = nullptr;       
      node2 -> prev = B;              
      B -> next = node2;             
      tail = node2;                   
      return;
   }
   if (( node1 -> next == node2 ) ) {
      node2 -> prev = B;  
      node2 -> next = node1;
      node1 -> prev = node2;
      node1 -> next = C;
   }
   if (( node2 -> next == node1 ) ) {
      node1 -> prev = D;
      node1 -> next = node2;
      node2 -> prev = node1;
      node2 -> next = A;
   }
   node1 -> next = C;
   node1 -> prev = D;
   node2 -> next = A;
   node2 -> prev = B;
}
Node* DoubleLinkedList::get_prev( const Node* currentNode ) const {
   return currentNode -> prev;;   
}
Node* DoubleLinkedList::get_last() const {
   return tail;
}

Node* DoubleLinkedList::pop_back() {
   if ( head == nullptr ) {
      return nullptr;
   }
   Node* temp = tail;         
   tail = temp -> prev;      
   tail -> next = nullptr;
   if ( head -> next == nullptr) {
      head = nullptr;
      tail = nullptr;
   }
   return temp;
}
void DoubleLinkedList::push_back( Node* newNode ) {
   newNode -> next = nullptr;   
   newNode -> prev = tail;       
   if ( head == nullptr ) {
      head = newNode;
      tail = newNode;
   }
   tail -> next = newNode;       
   tail = newNode;               
}
Node* DoubleLinkedList::get_next( const Node* currentNode ) const {
   return currentNode -> next;
}
Node* DoubleLinkedList::get_first() const {
   return head;

}
Node* DoubleLinkedList::pop_front() {
   if ( head == nullptr ) {
      return nullptr;
   }
   Node* temp = head;        
   head = temp -> next;     
   head -> prev = nullptr;
   if ( head -> next == nullptr) {
      head = nullptr;
      tail = nullptr;
   }
   return temp;
}
void DoubleLinkedList::push_front( Node* newNode ) {
   newNode -> prev = nullptr;   
   newNode -> next = head;       
   if ( head == nullptr ) {
      head = newNode;
      tail = newNode;
   }
   head -> prev = newNode;      
   head = newNode;               
}
const bool DoubleLinkedList::empty() const {
   if ( head == nullptr )
      return 1;     
   else
      return 0;    
}

