///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.hpp
/// @version 1.0
///
/// Header file for list
///
/// @author Dane Takenaka <daneht@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   4/6/2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "node.hpp"


class DoubleLinkedList {
   protected:
      Node* head = nullptr;
      Node* tail = nullptr;

   public:
      const bool empty() const;
      inline unsigned int size() const {
         // Initialize sum and temp Node
         unsigned int sum = 0;
         Node* temp = head;
         
         // Iterate to end of list and increment sum
         while( temp != nullptr ) {
            sum++;
            temp = temp -> next;
         }

         return sum;
      };

      void push_front( Node* newNode );
      void push_back( Node* newNode );
      Node* pop_front();
      Node* pop_back();

      void insert_after( Node* currentNode, Node* newNode );
      void insert_before( Node* currentNode, Node* newNode );

      const bool isIn( Node* aNode) const;

      void swap( Node* node1, Node* node2 );

      const bool isSorted() const;
      void insertionSort();

      Node* get_first() const;
      Node* get_next( const Node* currentNode ) const;
      Node* get_last() const;
      Node* get_prev( const Node* currentNode ) const;
      
      void dump() const;
};

